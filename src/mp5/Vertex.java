package mp5;

import java.util.ArrayList;

public class Vertex {

	private String name;
	public static ArrayList<String> vertexnames= new ArrayList <String>();
	public ArrayList<Edge> Edges= new ArrayList <Edge>();
	public ArrayList<Vertex> adjacentnodes= new ArrayList<Vertex>();
	public boolean visited=false;
	public static ArrayList<String> visitednodes = new ArrayList<String>();
	
	public Vertex(String name, Edge edge){
		this.name=name;
		Edges.add(edge);
		vertexnames.add(name);
		edge.addVertex(this);
		
	}
	
	public String getname(){
		return this.name;
	}
	public ArrayList<Edge> getEdges(){
		return Edges;
	}
	public ArrayList<String> getEdgenames(){
		ArrayList<String> edgenames = new ArrayList<String>();
		for(Edge e:Edges){
			edgenames.add(e.getname());
		}
		return edgenames;
	}
	public Edge getEdge(int index){
		Edge v=Edges.get(index);
	
		return v;
	}
	public void addEdge(Edge e){
		
		if(!e.verticies.contains(this)){
		Edges.add(e);
		e.addVertex(this);
		}
	
	}
	public boolean isvisited(){
		return visited;
	}
	public void setvisited(boolean v){
		this.visited=v;
		
		
	}
}

