package mp5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;



public class Graph {
	private static Map<Vertex, Edge> graph = new HashMap<Vertex, Edge>(); 
	public static ArrayList<Vertex> vertexlist= new ArrayList<Vertex>();
	private static ArrayList<Vertex> nextlevel= new ArrayList<Vertex>();
	private static Vertex currentnode;

	public static void populate () throws IOException{
		BufferedReader TSVFile = 
			    new BufferedReader(new FileReader("labeled_edges.txt")); // I changed the file to a text file so it would work properly with the buffered reader

			String dataRow = TSVFile.readLine(); // Read first line.

			while (dataRow != null){
			String[] dataArray = dataRow.split("\t");
			
			String r= dataArray[0];
			r= r.replaceAll("\"", ""); // get rid of the quotations 
			String r1=dataArray[1];
			r1= r1.replaceAll("\"", "");
			  System.out.println(r); 
			  System.out.println(r1); 
			  Edge e1= new Edge(r1,null);
			  Vertex v1= new Vertex(r,e1);
			  vertexlist.add(v1); // add all of the vertex onto a list alongside adding them onto a list<String> in the vertex class
			  // this makes it easier to convert from string to vertex since they will have the same index 
			  // proper way to do this would have been to use a double lookup table but it messes up other parts of the implementation
			 
			  graph.put(v1, e1);
			System.out.println(); // Print the data line.
			dataRow = TSVFile.readLine(); // Read next line of data.
		

			}
			connect();
}
	/**
	 * This class will make create an adjacency list which will give each vertex a list of neighbours
	 * A neighbour is defined if two verticies share the same edge (the same book)
	 * From here on the adjaceny list will be used to do computation. 
	 */
	public static void connect(){
		Vertex v1; 
		Vertex v2;
		int count = 0;
		
		for(int i = 0; i < vertexlist.size(); i++){
			v1 = vertexlist.get(i);
			for(int r = 0; r <vertexlist.size(); r++){
				v2 = vertexlist.get(r);
				String namev1 = v1.getEdgenames().get(0);
				String namev2 = v2.getEdgenames().get(0);
				
				if(namev1.equals(namev2)){
					v1.adjacentnodes.add(v2);
					v2.adjacentnodes.add(v1);
					count++;
				System.out.println(v1.adjacentnodes); // to show it works, comment out to make it go faster.	
				}
				
			}
			
			
		}
		
		TestMain.start();
	}
	
	/**
	 * This shortest path will analyze a the first level of the search tree and then look
	 * for the target on the subsequent neighbours of each element in the first level
	 * if not found within the first level or the neighbours of the first level, the next level
	 * will be defined with neighbours of the first level and the root node will be shifted to the 
	 * next level. Along the way, the route is marked and when the target is found, the marked route
	 * will be returned in the form of a string array containing the name of all the verticies in the
	 * path. This has worked on a smaller scale program I constructed outside of this, but there are still
	 * bugs remaining in this code. Note: The queuesearch method acts as a sort of recursive function call.
	 * 
	 */
	public static ArrayList<String> getpathq(Vertex v1, Vertex v2){
		 ArrayList<Vertex> level= new ArrayList<Vertex>(); 
		 ArrayList<String> AnswerinString = new ArrayList<String>();
	if(!v1.visited){
	level.addAll(v1.adjacentnodes);
	}
	else level.addAll(nextlevel);
		for(int i=0;i<level.size();i++){
			if(queuesearch(level.get(i), v2)){
				return Vertex.visitednodes;
			}
			else level.get(i).setvisited(true);
		}
		int size=Vertex.visitednodes.size();
		for(int i=0;i<size;i++){
			String s=Vertex.visitednodes.get(i);
			AnswerinString.add(s);
		}
	return AnswerinString; 
	}
	
	public static boolean queuesearch(Vertex q, Vertex target){
		Queue<Vertex> queue = new LinkedList<Vertex>();
		queue.addAll(q.adjacentnodes);
		for(int i=0;i<queue.size();i++){
			currentnode=queue.remove();
			if(currentnode==target){
				return true;
			}
			else nextlevel.add(currentnode);
			
			
		}
		getpathq(q,target);
		return false;
	}
}
