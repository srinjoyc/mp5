package mp5;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Edge {

	public static ArrayList<String> Edgenames= new ArrayList <String>();
	public ArrayList<Vertex> verticies= new ArrayList <Vertex>();
	
	private String name;
	
	public Edge(String name, Vertex vertex){
		this.name=name;
		verticies.add(vertex);
		Edgenames.add(name);
		if(!(vertex==null)){
		vertex.addEdge(this);
		}
		
	}
	
	public String getname(){
		return this.name;
	}
	public ArrayList<Vertex> getvertecies(){
		return verticies;
	}
	public ArrayList<String> getverteciesnames(){
		ArrayList<String> vertexnames = new ArrayList<String>();
		for(Vertex e:verticies){
			if(e==null){
				vertexnames.add("null");
			}
			else
			vertexnames.add(e.getname());
		}
		return vertexnames;
	}
	public Vertex getvertex(int index){
		Vertex v=verticies.get(index);
	
		return v;
	}
	public void addVertex(Vertex v){
		if(!v.Edges.contains(this)){
			verticies.add(v);
			v.addEdge(this);
		}
				
	}
	
	public static void main (String [] args){
		Edge edge1= new Edge("AA2 35",null);
		Vertex character = new Vertex("FROST, CARMILLA", edge1);
		edge1.getname();
	}
}
	

